//
//  JSONService.swift
//  Simbirsoft
//
//  Created by Евгений Прохоров on 09.04.2023.
//

import Foundation

class JSONService {
    func getJsonString<T: Codable>(filename fileName: String, model: T.Type) -> T? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let jsonData = try decoder.decode(T.self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}
