//
//  MainScreenViewModel.swift
//  Simbirsoft
//
//  Created by Евгений Прохоров on 10.04.2023.
//

import Foundation

class MainScreenViewModel: MainScreenViewModelProtocol {
    var data: [Task] = []
    let jsonParser = JSONService()
    
    func fetchTasks(date: Date) {
        let data = jsonParser.getJsonString(filename: "mockData", model: [Task].self)
        guard let data else { return }
        
        self.data = data
            .filter { task in
                return date.hasSame(.day, as: task.dateStart.toDate()) && date.hasSame(.day, as: task.dateFinish.toDate())
            }
            .map { task in
                return Task(id: task.id,
                            dateStart: task.dateStart,
                            dateFinish: task.dateFinish,
                            name: task.name,
                            description: task.description)
        }
    }
}
