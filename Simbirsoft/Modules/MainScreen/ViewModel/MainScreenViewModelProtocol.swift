//
//  MainScreenViewModelProtocol.swift
//  Simbirsoft
//
//  Created by Евгений Прохоров on 16.04.2023.
//

import Foundation

protocol MainScreenViewModelProtocol {
    var data: [Task] { get set }
    func fetchTasks(date: Date)
}
