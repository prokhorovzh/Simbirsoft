//
//  MainScreenViewController.swift
//  Simbirsoft
//
//  Created by Евгений Прохоров on 10.04.2023.
//

import UIKit
import FSCalendar

class MainScreenViewController: UIViewController {
    let viewModel: MainScreenViewModel
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .white
        tableView.register(TableViewCell.self, forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    private lazy var calendar: FSCalendar = {
        let calendar = FSCalendar(frame: CGRect(x: 0, y: 50, width: view.frame.width, height: 300))
        calendar.dataSource = self
        calendar.delegate = self
        return calendar
    }()
    
    init(viewModel: MainScreenViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        getTodayTasks()
    }
    
    private func setupView() {
        view.backgroundColor = .white
        view.addSubview(calendar)
        view.addSubview(tableView)
    }
    
    private func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(calendar.snp.bottom)
        }
    }
    
    private func getTodayTasks() {
        guard let today = calendar.today else { return }
        viewModel.fetchTasks(date: today)
        tableView.reloadData()
    }
}

extension MainScreenViewController: FSCalendarDelegate, FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        viewModel.fetchTasks(date: date)
        tableView.reloadData()
    }
}

extension MainScreenViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TableViewCell {
            cell.configure(task: viewModel.data[indexPath.row])
            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsViewModel = DetailsViewModel(task: viewModel.data[indexPath.row])
        let nextView = DetailsViewController(viewModel: detailsViewModel)
        nextView.modalTransitionStyle = .coverVertical
        nextView.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(nextView, animated: true)
    }
}
