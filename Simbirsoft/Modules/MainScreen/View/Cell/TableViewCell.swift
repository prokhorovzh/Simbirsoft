//
//  TableViewCell.swift
//  Simbirsoft
//
//  Created by Евгений Прохоров on 11.04.2023.
//

import UIKit
 import SnapKit

final class TableViewCell: UITableViewCell {
    
    lazy var leftTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        return label
    }()
    
    private lazy var rightTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.textAlignment = .right
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .white
        addSubviews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        [
            leftTitleLabel,
            rightTitleLabel
        ].forEach { addSubview($0) }
    }
    
    private func setConstraints() {
        leftTitleLabel.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview().inset(16)
        }

        rightTitleLabel.snp.makeConstraints { make in
            make.leading.equalTo(leftTitleLabel.snp.trailing).offset(16)
            make.top.bottom.trailing.equalToSuperview().inset(16)
        }
    }
    
    func configure(task: Task) {
        leftTitleLabel.text = task.name
        rightTitleLabel.text = "\(task.dateStart.getTime()) - \(task.dateFinish.getTime())"
    }
}
