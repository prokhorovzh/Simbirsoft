//
//  DetailsView.swift
//  Simbirsoft
//
//  Created by Евгений Прохоров on 15.04.2023.
//

import UIKit
import SnapKit

class DetailsView: UIView {
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 28)
        label.textColor = .black
        return label
    }()
    
    private lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 20, weight: .regular)
        label.numberOfLines = 2
        label.textColor = .black
        return label
    }()
    
    private lazy var descriptionTextView: UITextView = {
        let textView = UITextView()
        textView.font = .systemFont(ofSize: 16, weight: .regular)
        textView.backgroundColor = .gray
        textView.textColor = .white
        textView.isEditable = false
        textView.layer.cornerRadius = 8
        textView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        return textView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        
        [
            nameLabel,
            timeLabel,
            descriptionTextView
        ].forEach { addSubview($0) }
    }
    
    private func setupConstraints() {
        
        nameLabel.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview().inset(16)
        }
        
        timeLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(nameLabel.snp.bottom).offset(8)
        }
        
        descriptionTextView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview().inset(16)
            make.top.equalTo(timeLabel.snp.bottom).offset(16)
        }
    }
    
    public func configure(data: Task) {
        nameLabel.text = data.name
        timeLabel.text = "Дата: \(data.dateStart.getDateDisplayString())\nВремя: \(data.dateStart.getTime()) - \(data.dateFinish.getTime())"
        descriptionTextView.text = data.description
    }
}
