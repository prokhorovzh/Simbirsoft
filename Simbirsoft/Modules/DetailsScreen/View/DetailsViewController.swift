//
//  DetailsViewController.swift
//  Simbirsoft
//
//  Created by Евгений Прохоров on 15.04.2023.
//

import UIKit
import SnapKit

class DetailsViewController: UIViewController {
    let viewModel: DetailsViewModel
    
    private lazy var detailsView = DetailsView()
    
    init(viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }
    
    private func setupView() {
        view.backgroundColor = .white
        title = viewModel.task.name
        view.addSubview(detailsView)
        detailsView.configure(data: viewModel.task)
    }
    
    private func setupConstraints() {
        detailsView.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaLayoutGuide)
        }
    }
}
