//
//  DetailsViewModel.swift
//  Simbirsoft
//
//  Created by Евгений Прохоров on 15.04.2023.
//

import Foundation

class DetailsViewModel {
    var task: Task
    
    init(task: Task) {
        self.task = task
    }
}
