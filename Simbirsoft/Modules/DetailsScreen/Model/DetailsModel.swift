//
//  MainScreenModel.swift
//  Simbirsoft
//
//  Created by Евгений Прохоров on 10.04.2023.
//

import Foundation

struct Task: Codable {
    let id: Int
    let dateStart: String
    let dateFinish: String
    let name: String
    let description: String
}
