//
//  SimbirsoftUnitTests.swift
//  SimbirsoftTests
//
//  Created by Евгений Прохоров on 17.04.2023.
//

import XCTest
@testable import Simbirsoft

final class SimbirsoftUnitTests: XCTestCase {
    
    var data: MainScreenViewModel!

    override func setUpWithError() throws {
        try super.setUpWithError()
        self.data = MainScreenViewModel()
    }

    override func tearDownWithError() throws {
        self.data = nil
        try super.tearDownWithError()
        
    }

    func testExample() throws {
        let date: Date = Date()
        
        self.data.fetchTasks(date: date)
        XCTAssert(!self.data.data.isEmpty, "Статус появления даты")
        
    }

    func testPerformanceExample() throws {
        let date: Date = Date()
        self.measure(
            metrics: [
                XCTClockMetric(),
                XCTCPUMetric(),
                XCTStorageMetric(),
                XCTMemoryMetric()
            ]) {
            self.data.fetchTasks(date: date)
        }
    }
}
